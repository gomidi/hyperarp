# hyperarp

A special breed of MIDI arpeggiator.


## Installation

```sh
$ go install gitlab.com/gomidi/hyperarp
```
  
## Usage

```sh
$ hyperarp help

hyperarp v0.0.22
  hyper arpeggiator

usage:
  hyperarp [command] OPTION...

options:
  -i, --in=<integer>            number of the input MIDI port (use hyperarp
                                list to see the available MIDI ports)

  -o, --out=<integer>           number of the output MIDI port (use hyperarp
                                list to see the available MIDI ports)

  [-t, --transpose=0]           transpose (number of semitones)

  [-b, --tempo=120]             tempo (BPM)

  [--ccdir=80]                  controller number for the direction switch

  [--cctiming=16]               controller number to set the timing interval

  [--ccstyle=17]                controller number to select the playing style
                                (staccato, non-legato, legato)

  [--notedir=<integer>]         note (key) for the direction switch

  [--notetiming=<integer>]      note (key) for the timing interval

  [--notestyle=<integer>]       note (key) for the playing style (staccato,
                                non-legato, legato)

  [--ctrlch=<integer>]          channel for control messages (only needed if
                                not the same as the input channel


commands:
  list                          show the available MIDI ports

```
